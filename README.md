# Chore infrastructure

Ansible scripts for all my chore servers

# Production deploy
```bash
ansible-playbook -K -i production site.yml
```

## Partial deploy
```bash
ansible-playbook -K -i production site.yml --limit dhcp_server
```  
or  
```bash
ansible-playbook -K -i production common.yml
```

# Testing

The staging inventory can be used to test the ansible scripts against a testing server

1. Set the ip of your test server in the [staging host var file](host_vars/staging_test.yml)
2. ```ansible-playbook --ask-vault-pass -i staging site.yml```


## Set up staging environment
Best way of testing is with a lxd container, but the lxd ansible connection plugin is not really working because it depends on old python2 and libraries.  
So ssh to the lxd container is the way to go.

An ubuntu lxd container needs at least the following configuration to work
```bash
passwd ubuntu
sudo su ubuntu
# remove default PasswordAuthentication
echo PasswordAuthentication yes | sudo tee -a /etc/ssh/sshd_config
sudo systemctl restart sshd.service
```

> Hint:  
You don't need to redo this every time to get a new lxc instance. You can also do it once and create a snapshot of the container afterwards.  
```bash
# Create snapshot
lxc snapshot AnsibleTestServer <snapshot_name>

# Restore snapshot
lxc restore AnsibleTestServer <snapshot_name>
```
